
```mermaid
classDiagram


class Note{
    id: int PK
    title: String
    content: String
    deadline: Date
    creator: int FK (user id)
    color: String
    participants : int FK (user id)
    created_at: Date
    updated_at: Date
}

class User{
    id: int PK
    username: string
    email: string
    password: string
    involvement: int FK (note id)
    picture: int FK (picture id)
}

class Role{
    id: int PK
    name: String
}

class Notification{
    id: int PK
    from: int FK(user id)
    to: int FK (user id)
    note: int FK
}



class File{
    id: PK
    name: string
    posted_at: Date
    posted_by: int FK (user id)
}

class Picture{
    id: PK
    posted_at: Date,
    user: int FK (user id)
}

Note "*" --> "*" User
Role "1" --> "*" User
Notification "*" --> "*" User
File "*" --> "*" User
Picture "*" --> "1" User
