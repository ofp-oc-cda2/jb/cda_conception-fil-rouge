# Flow Chart
```mermaid
graph TD
    A[USER WITHOUT ACCOUNT] --> B(Access)
    B --> C[Consult the notes]
    B --> D[Consult the files]
```

```mermaid
graph TD
    A[USER WITH ACCOUNT] -->|Login| B(Access)
    B --> C[Access notes] --> D[Create note, update one, delete notes, invite other users]
    B --> E[Consult his profil] --> F[Update profil informations]
    B --> G[Consult, upload and download files]
```

```mermaid
graph TD
    A[ADMIN] -->|Login| B(Access)
    B --> C[Access notes] --> D[Create note, update one, delete notes, invite other users]
    B --> E[Consult his profil] --> F[Update profil informations]
    B --> G[Access admin panel] --> H[Consult the list of users] --> I[Modify informations about other users]
    H --> J[Create account for other and define role]
    B --> K[Consult, upload and download files, delete files]
```
